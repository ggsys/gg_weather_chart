require 'rufus/scheduler'
require "httparty"

  syn_data = Rufus::Scheduler.new
  syn_data.every '100m' do
    Rails.logger.info "Synoptic weather data start------------"
    #获取项目(有经纬度自定义属性的),经纬度数值不能为空
    projects = Project.where("id in (select customized_id from custom_values where custom_field_id in (select id from custom_fields where type='ProjectCustomField' and name='经度') and value !='')
                                and
                              id in (select customized_id from custom_values where custom_field_id in (select id from custom_fields where type='ProjectCustomField' and name='纬度') and value !='')")
    if projects.length > 0
      #循环每一个项目
      projects.each do |project|
        #分别取出项目的"经度"和"纬度"数值(必需项)
        longitude = CustomValue.find_by("customized_id = ? and custom_field_id in (select id from custom_fields where type='ProjectCustomField' and name='经度')",project[:id].to_s)
        latitude = CustomValue.find_by("customized_id = ? and custom_field_id in (select id from custom_fields where type='ProjectCustomField' and name='纬度')",project[:id].to_s)
        #通过经纬度获取行政区编码(adcode)
        url = "https://restapi.amap.com/v3/geocode/regeo?key=f9c80cf3eef3fded8cabd6d92954174b&location="+longitude[:value].to_s+","+latitude[:value].to_s
        body = HTTParty.get(URI.escape(url)).body
        adcode = JSON.parse(body)["regeocode"]["addressComponent"]["adcode"]
        #通过行政区编码查询当地当天的天气状况
        url_2 = "https://restapi.amap.com/v3/weather/weatherInfo?key=f9c80cf3eef3fded8cabd6d92954174b&extensions=all&city="+adcode
        weather_body = HTTParty.get(URI.escape(url_2)).body
        if weather_body
        # weather = GgWeatherCharts.new
        forecasts = JSON.parse(weather_body)["forecasts"]
        info = forecasts.first
        Rails.logger.info(info)
        # casts = info["casts"].first
        # weather[:date] = casts["date"]
        # weather[:day_temp] = casts["daytemp"]
        # weather[:day_weather] = casts["dayweather"]
        # weather[:day_wind] = casts["daywind"]
        # weather[:night_temp] = casts["nighttemp"]
        # weather[:night_weather] = casts["nightweather"]
        # weather[:night_wind] = casts["nightwind"]
        # weather[:week] = casts["week"]
        # weather.save
        end
      end
    end
    Rails.logger.info "Synoptic weather data end------------"
  end

# require "rest-client"
#
# syn_data = Rufus::Scheduler.new
# syn_data.every '3m' do
#   Rails.logger.info "Synoptic weather data start------------"
#   url = "https://restapi.amap.com/v3/weather/weatherInfo?key=f9c80cf3eef3fded8cabd6d92954174b&extensions=all&city=济南"
#   response = RestClient.get(URI.escape(url))
#   Rails.logger.info(response.body)
#   Rails.logger.info "Synoptic weather data end------------"
# end