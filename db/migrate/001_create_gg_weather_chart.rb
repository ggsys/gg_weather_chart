class CreateGgWeatherChart < ActiveRecord::Migration
  def change
    create_table :gg_weather_charts do |t|
      t.integer :project_id
      t.date :date
      t.string :day_temp
      t.string :day_weather
      t.string :day_wind
      t.string :night_temp
      t.string :night_weather
      t.string :night_wind
      t.integer :week
    end
  end
end