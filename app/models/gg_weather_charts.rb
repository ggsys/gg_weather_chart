class GgWeatherCharts < ActiveRecord::Base
  include Redmine::SafeAttributes
  serialize   :conditions, JSON

  safe_attributes 'project_id','date','day_temp','day_weather','day_wind','night_temp','night_weather','night_wind','week'
  attr_accessible :project_id, :date,:day_temp,:day_weather,:day_wind, :night_temp, :night_weather,:night_wind,:week

end