require_dependency File.dirname(__FILE__) + '/lib/gg_weather_chart_b.rb'

Redmine::Plugin.register :gg_weather_chart do
  name 'Weatherchart plugin'
  author 'Author name'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'

  project_module :gg_weather_chart do
    permission :gg_weather_chart_view,
               {:gg_weather_chart => [ :index,:show]
               },
               :require => :member
    permission :gg_weather_chart_edit,
               {:gg_weather_chart => [ :index, :show, :new, :create, :update, :edit, :delete]
               },
               :require => :member
  end

  menu :project_menu, :gg_weather_chart, {:controller => 'gg_weather_chart', :action => 'show' }, :caption => :label_weather_chart ,
       :before => :setting, :param =>:project_id

end
