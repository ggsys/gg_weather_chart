# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html

RedmineApp::Application.routes.draw do
  match "/projects/:project_id/weather_chart/show" => 'gg_weather_chart#show', :as => :gg_weather_chart_show, via:[:get]

  match "/projects/:project_id/weather_chart/info/new" => 'gg_weather_chart#new', via:[:post]
end

